<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ne04j-BOM</title>
 <script src="<%=request.getContextPath()%>/resources/sockjs-0.3.4.js"></script>
 <script src="<%=request.getContextPath()%>/resources/stomp.js"></script>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/CSS/etl-neo.css" />
<script src="<%=request.getContextPath()%>/resources/Plugins/d3.v3.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/resources/Plugins/dagre-d3.js"></script>
<script src="<%=request.getContextPath()%>/resources/Plugins/jquery-1.8.0.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/JS/etl-data-neo.js"></script>
<script src="<%=request.getContextPath()%>/resources/JS/etl-tools-neo.js"></script>
  <script type="text/javascript">
	
        var stompClient = null; 

        function setConnected(connected) {
            //document.getElementById('connect').disabled = connected;
            //document.getElementById('disconnect').disabled = !connected;
            //document.getElementById('commentDiv').style.visibility = connected ? 'visible' : 'hidden';
            //document.getElementById('commentLst').innerHTML = '';
        }

        function connect() {
            //var socket = new SockJS('/ETLTopology/partList');
            //stompClient = Stomp.over(socket);
            var socket = "ws://localhost:8080//ETLTopology/partList";
            stompClient = Stomp.client(socket);
            stompClient.connect({}, function(frame) {
                setConnected(true);
                console.log('Connected: ' + frame);
                stompClient.subscribe("/topic/partList", function(message) {
               	 console.log("parts message received is",message);
                   //self.portfolio().processQuote(JSON.parse(message.body));
                   var data = JSON.parse(message.body);
               	 partDatabase.updatePartGraph(data);
               });
                stompClient.subscribe('/app/partList', function(calResult){
                    console.log("message received is == part",calResult);
                	//showResult(JSON.parse(calResult.body));
                   // var data = JSON.parse(message.body);
               	 	//partDatabase.updatePartGraph(data);
                });
                
            });
        }
	 function disconnect() {
            stompClient.disconnect();
            setConnected(false);
            console.log("Disconnected");
        }

        function addComment() {
            var commentId = document.getElementById('commentId').value;
            var commentDesc = document.getElementById('commentDesc').value;
            var author = document.getElementById('commentAuthor').value;
            stompClient.send("/comments/addComment", {}, JSON.stringify({ 'commentId': commentId, 'commentDesc': commentDesc ,'author':author}));
        }

        window.updatePartStatus = function(partId,transType) {
            /* var commentId = document.getElementById('commentId').value;
            var commentDesc = document.getElementById('commentDesc').value;
            var author = document.getElementById('commentAuthor').value; */
            // stompClient.send("/comments/addComment", {}, JSON.stringify({ 'commentId': commentId, 'commentDesc': commentDesc ,'author':author}));
            console.log('part id is', partId);
            console.log('transType is', transType);
            $.ajax({
              url: "/ETLTopology/updatePart",
              data: partId+":"+transType,
              contentType: "application/json",
              Accept: "application/json",
              type: "POST"
            }).done(function(data) {
              console.log("part is updated",data);
            });
        }

        function showResult(message) {
            console.log("Message received",message);
            var response = document.getElementById('jobList');
            var p = document.createElement('p');   
            p.style.wordWrap = 'break-word'; 
            p.appendChild(document.createTextNode(message));
            response.appendChild(p);
           // var p = document.createElement('p');
           // p.style.wordWrap = 'break-word';
//             for (var i in message){
//             var p = document.createElement('p');   
//             p.style.wordWrap = 'break-word'; 
            //p.appendChild(document.createTextNode(message[i].commentDesc));
            //p.appendChild(document.createTextNode(message[i].author));
//             response.appendChild(p);
//         	}
        	
        }
        connect();
    </script>
</head>
<body>
<noscript><h2>Enable Java script and reload this page to run Websocket Demo</h2></noscript>
<div>
    <!-- <div>
        <button id="connect" onclick="connect();">Connect</button>
        <button id="disconnect" disabled="disabled" onclick="disconnect();">Disconnect</button><br/><br/>
    </div>
    <div id="jobList">
    
        <p id="jobStatusMesg"></p>
    </div> -->
    <div class="editPopUp">
    <div class="popUpDiv">
     Order Status : <br>
       <select >
        <option value="ordered">Ordered</option>
       <!--   <option value="inProgress">In Progress</option>
        <option value="fail">Fail</option>-->
        <option value="pending">Pending</option>
      </select>
      <br>
      Remarks : <input type="text" class="statusInput" id="statusMessage"><br>
    </div>
  </div>
    <div class="titleDiv">
      Product Topology Visualizer
    </div>
  <div class="mainBody">
  
    <div class="live map">
      <svg>
        <filter id="dropshadow" height="130%">
          <feGaussianBlur in="SourceAlpha" stdDeviation="1"/> <!-- stdDeviation is how much to blur -->
          <feOffset dx="1" dy="1" result="offsetblur"/> <!-- how much to offset -->
          <feMerge> 
            <feMergeNode/> <!-- this contains the offset blurred image -->
            <feMergeNode in="SourceGraphic"/> <!-- this contains the element that the filter is applied to -->
          </feMerge>
        </filter>
        <g/>

      </svg>
    </div>
  </div>
  <div class="legend-box">
    <div class="items">
      <div class="view">
        <div class="circle ordered"></div>
      </div>
      <div class="text">Ordered</div>
    </div>
    <!--  <div class="items">
      <div class="view">
        <div class="circle inProgress"></div>
      </div>
      <div class="text">In Progress</div>
    </div>
    <div class="items">
      <div class="view">
        <div class="circle fail"></div>
      </div>
      <div class="text">Fail</div>
    </div>-->
    <div class="items">
      <div class="view">
        <div class="circle pending"></div>
      </div>
      <div class="text">Pending</div>
    </div>

  </div>
  

  <script src="<%=request.getContextPath()%>/resources/JS/etl-final-neo.js"></script>
</div>

</body>
</html>