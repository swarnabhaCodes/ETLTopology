var getEtlData = function(){

  var workers = {
    "identifier": {
      "consumers": 2,
      "count": 20
    },
    "lost-and-found": {
      "consumers": 1,
      "count": 1,
      "inputQueue": "identifier",
      "inputThroughput": 50
    },
    "monitor": {
      "consumers": 1,
      "count": 0,
      "inputQueue": "identifier",
      "inputThroughput": 50
    },
    "meta-enricher": {
      "consumers": 4,
      "count": 9900,
      "inputQueue": "identifier",
      "inputThroughput": 50
    },
    "geo-enricher": {
      "consumers": 2,
      "count": 1,
      "inputQueue": "meta-enricher",
      "inputThroughput": 50
    },
    "elasticsearch-writer": {
      "consumers": 0,
      "count": 9900,
      "inputQueue": "geo-enricher",
      "inputThroughput": 50
    },
    "funnyShow": {
      "consumers": 0,
      "count": 9900,
      "inputQueue": "geo-enricher",
      "inputThroughput": 50
    }

  };

  return workers;

};

var rawArray = [
  {
    "completionDesc": "Job1_name",
    "completionStatus": "completed",
    "jobName": "Job1_name",
    "jobId": "Job1",
    "parentJobId": "root",
    "completionPercent": 100,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "Job2_name",
    "completionStatus": "inProgress",
    "jobName": "Job2_name",
    "jobId": "Job2",
    "parentJobId": "Job1",
    "completionPercent": 20,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "Job3_name",
    "completionStatus": "inProgress",
    "jobName": "Job3_name",
    "jobId": "Job3",
    "parentJobId": "Job1",
    "completionPercent": 10,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "Job4_name",
    "completionStatus": "fail",
    "jobName": "Job4_name",
    "jobId": "Job4",
    "parentJobId": "Job1",
    "completionPercent": 50,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "Job5_name",
    "completionStatus": "pending",
    "jobName": "Job5_name",
    "jobId": "Job5",
    "parentJobId": "Job4",
    "completionPercent": 0,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "Job6_name",
    "completionStatus": "pending",
    "jobName": "Job6_name",
    "jobId": "Job6",
    "parentJobId": "Job4",
    "completionPercent": 0,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "Job7_name",
    "completionStatus": "pending",
    "jobName": "Job7_name",
    "jobId": "Job7",
    "parentJobId": "Job4",
    "completionPercent": 0,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "Job8_name",
    "completionStatus": "pending",
    "jobName": "Job8_name",
    "jobId": "Job8",
    "parentJobId": "Job5",
    "completionPercent": 0,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "Job9_name",
    "completionStatus": "pending",
    "jobName": "Job9_name",
    "jobId": "Job9",
    "parentJobId": "Job3",
    "completionPercent": 0,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "job10_name",
    "completionStatus": "pending",
    "jobName": "Job10_name",
    "jobId": "Job10",
    "parentJobId": "Job9",
    "completionPercent": 0,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "job11_name",
    "completionStatus": "pending",
    "jobName": "Job11_name",
    "jobId": "Job11",
    "parentJobId": "Job7",
    "completionPercent": 0,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  },
  {
    "completionDesc": "job12_name",
    "completionStatus": "pending",
    "jobName": "Job12_name",
    "jobId": "Job12",
    "parentJobId": "Job2",
    "completionPercent": 0,
    "configuredETA": "Sun Aug 23 2015 00:47:25",
    "completedTime": "Sun Aug 23 2015 00:47:25"
  }
];


var childObj = {
  root: {
      "completedTime": "Sun Aug 23 2015 00:42:25",
      "configuredETA": "Sun Aug 23 2015 00:45:25",
      "completionPercent": 100,
      "jobName": "root",
      "completionStatus": "completed",
      "jobId": "root",
      "completionDesc": "root"
    }
  };

  var parentArrObj = {};
  var hierachyDataObj = {
    root: childObj.root
  };

  // var funCalculateETA = function(childrenData, hierachyDataObj, parentName){
  //     var parentData = hierachyDataObj[parentName];
      
  //     for (var i = 0; i < childrenData.length; i++) {
  //       var currentNode = hierachyDataObj[childrenData[i].jobId];
  //       currentNode['calculatedETA'] = currentNode['runningETA']+parentData.calculatedETA;
  //       parentName = currentNode.jobId;
  //       var subChildren = parentArrObj[parentName];
  //       if(subChildren){
  //         funCalculateETA(subChildren, hierachyDataObj, parentName);
  //       }
  //     }

  //     return hierachyDataObj;
  // };
  

var getConvertedData = function(rawArray){
//    rawArray.forEach(function(d, i){
//      if(!parentArrObj[d.parentJobId]){
//        parentArrObj[d.parentJobId] = [d];
//      }else{
//        parentArrObj[d.parentJobId].push(d);
//      }
//      hierachyDataObj[d.jobId] = d;
//    });
//    var parentName = 'root';
//    var childrenData = parentArrObj[parentName];
//    var calculatedETAObj = funCalculateETA(childrenData, hierachyDataObj, parentName);

    rawArray.forEach(function(d, i){
      hierachyDataObj[d.jobId] = d;
      childObj[d.jobId] = { 
        parentJobId: d.parentJobId,  
        jobId: d.jobId,
        completionPercent: d.completionPercent,
        completedTime: d.completedTime,
        configuredETA: d.configuredETA,
        completionStatus: d.completionStatus,
        jobName: d.jobName,
        completionDesc: d.completionDesc
      };
    });

    // if(!parentArrObj[d.parent]){
    //     parentArrObj[d.parent] = [d];
    //   }else{
    //     parentArrObj[d.parent].push(d);
    //   }

    return childObj;
};

var findParents = function(childName){

};

var updateEtlGraph = function(rData){
	var workers = etlDatabase.getConvertedData(rData);
	updateGraph(workers);
}


window.etlDatabase = {
  getEtlData: getEtlData,
  getConvertedData: getConvertedData,
  updateEtlGraph: updateEtlGraph
};

