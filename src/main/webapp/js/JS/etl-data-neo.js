var getPartData = function() {

	var workers = {
		"identifier" : {
			"consumers" : 2,
			"count" : 20
		},
		"lost-and-found" : {
			"consumers" : 1,
			"count" : 1,
			"inputQueue" : "identifier",
			"inputThroughput" : 50
		},
		"monitor" : {
			"consumers" : 1,
			"count" : 0,
			"inputQueue" : "identifier",
			"inputThroughput" : 50
		},
		"meta-enricher" : {
			"consumers" : 4,
			"count" : 9900,
			"inputQueue" : "identifier",
			"inputThroughput" : 50
		},
		"geo-enricher" : {
			"consumers" : 2,
			"count" : 1,
			"inputQueue" : "meta-enricher",
			"inputThroughput" : 50
		},
		"elasticsearch-writer" : {
			"consumers" : 0,
			"count" : 9900,
			"inputQueue" : "geo-enricher",
			"inputThroughput" : 50
		},
		"funnyShow" : {
			"consumers" : 0,
			"count" : 9900,
			"inputQueue" : "geo-enricher",
			"inputThroughput" : 50
		}

	};

	return workers;

};

//var rawArray1 = function () {
//	 $.ajax({
//         url: "/ETLTopology/getParts",
//         contentType: "application/json",
//         Accept: "application/json",
//         type: "GET",
//         async: false
//       }).done(function(data) {
//         console.log("in rawArray1 part is updated",data);
//         console.log("parsing json",JSON.parse(data));
//         return  JSON.parse(data);
//       });
//};
//console.log("rawArray1=============",rawArray1());
/*var rawArray = [
                {
                    "partId": "1",
                    "partName": "iphnP1",
                    "parentPartId": "root",
                    "price": "300",
                    "level": "1",
                    "partDesc": "Housing Unit",
                    "orderStatus": "pending"
                },
                {
                    "partId": "2",
                    "partName": "iphnP2",
                    "parentPartId": "root",
                    "price": "600",
                    "level": "1",
                    "partDesc": "Chip Unit",
                    "orderStatus": "pending"
                },
                {
                    "partId": "3",
                    "partName": "iphnP3",
                    "parentPartId": "root",
                    "price": "100",
                    "level": "1",
                    "partDesc": "SD Card Holder",
                    "orderStatus": "pending"
                },
                {
                    "partId": "4",
                    "partName": "iphnP4",
                    "parentPartId": "1",
                    "price": "200",
                    "level": "2",
                    "partDesc": "SIM Card Holder",
                    "orderStatus": "pending"
                },
                {
                    "partId": "5",
                    "partName": "iphnP5",
                    "parentPartId": "4",
                    "price": "10",
                    "level": "3",
                    "partDesc": "SIM Gold Contact",
                    "orderStatus": "pending"
                },
                {
                    "partId": "6",
                    "partName": "iphnP6",
                    "parentPartId": "2",
                    "price": "700",
                    "level": "2",
                    "partDesc": "A1 Chip",
                    "orderStatus": "pending"
                },
                {
                    "partId": "7",
                    "partName": "iphnP7",
                    "parentPartId": "2",
                    "price": "50",
                    "level": "2",
                    "partDesc": "Chip Bus Spec2",
                    "orderStatus": "pending"
                }
            ];*/

//rawArray = rawArray1();
var rawArray = [];
var childObj = {
	root : {
		"level" : 0,
		"partName" : "root",
		"price" : "2000",
		"partId" : "root",
		"partDesc" : "ROOT",
		"orderStatus":"pending"

	}
};
var parentArrObj = {};
var hierachyDataObj = {
	root : childObj.root
};
// var funCalculateETA = function(childrenData, hierachyDataObj, parentName){
// var parentData = hierachyDataObj[parentName];

// for (var i = 0; i < childrenData.length; i++) {
// var currentNode = hierachyDataObj[childrenData[i].jobId];
// currentNode['calculatedETA'] =
// currentNode['runningETA']+parentData.calculatedETA;
// parentName = currentNode.jobId;
// var subChildren = parentArrObj[parentName];
// if(subChildren){
// funCalculateETA(subChildren, hierachyDataObj, parentName);
// }
// }

// return hierachyDataObj;
// };

var getConvertedData = function(rawArray) {
	console.log("raw data for conversion",rawArray);
	// rawArray.forEach(function(d, i){
	// if(!parentArrObj[d.parentJobId]){
	// parentArrObj[d.parentJobId] = [d];
	// }else{
	// parentArrObj[d.parentJobId].push(d);
	// }
	// hierachyDataObj[d.jobId] = d;
	// });
	// var parentName = 'root';
	// var childrenData = parentArrObj[parentName];
	// var calculatedETAObj = funCalculateETA(childrenData, hierachyDataObj,
	// parentName);
	
	rawArray.forEach(function(d, i) {
		hierachyDataObj[d.partId] = d;
		childObj[d.partId] = {
			parentPartId : d.parentPartId,
			partId : d.partId,
			price : d.price,
			partName : d.partName,
			partDesc : d.partDesc,
			level : d.level,
			orderStatus : d.orderStatus,
		};
	});

	// if(!parentArrObj[d.parent]){
	// parentArrObj[d.parent] = [d];
	// }else{
	// parentArrObj[d.parent].push(d);
	// }

	return childObj;
};

var findParents = function(childName) {

};

var updatePartGraph = function(rData) {
	var workers = partDatabase.getConvertedData(rData);
	console.log("workers are",workers);
	updateGraph(workers);
}

window.partDatabase = {
	getPartData : getPartData,
	getConvertedData : getConvertedData,
	updatePartGraph : updatePartGraph
};
