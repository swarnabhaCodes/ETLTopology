var rawArray1 = function () {
	 $.ajax({
         url: "/ETLTopology/getParts",
         contentType: "application/json",
         Accept: "application/json",
         type: "GET",
         async: false
       }).done(function(data) {
         console.log("in rawArray1 part is updated",data);
         console.log("parsing json",JSON.parse(data));
         return  JSON.parse(data);
       });
};
//rawArray = rawArray1();

var workers = partDatabase.getConvertedData(rawArray);
var nodeName = '';
  // Set up zoom support
  var svg = d3.select("svg"),
      inner = svg.select("g"),
      zoom = d3.behavior.zoom().on("zoom", function() {
        inner.attr("transform", "translate(" + d3.event.translate + ")" +
                                    "scale(" + d3.event.scale + ")");
      });
  svg.call(zoom);

  var render = new dagreD3.render();

  // Left-to-right layout
  var g = new dagreD3.graphlib.Graph();
  g.setGraph({
    nodesep: 70,
    ranksep: 50,
    rankdir: "LR",
    marginx: 20,
    marginy: 20
  });

  function draw(isUpdate) {
	  console.log("drawing the chart",workers);
    for (var id in workers) {
      var worker = workers[id];
      var className =  "running" ;
      if (worker.count > 10000) {
        className += " warn";
      }
      var statusClass = worker.orderStatus ? worker.orderStatus: '' ;
      var html = '<div class=textDiv data-name="'+worker.partId+'" data-desc="'+worker.partDesc+'">';
       html += "<span class=queue><span class=name ><b>"+worker.partDesc+"</b></span></span>";
      html += "<span class=queue><span class=consumers><b>Price (USD):</b> "+worker.price+"</span></span>";
	  html += "<span class=queue><span class=consumers><b>Order Status:</b> "+worker.orderStatus+"</span></span>";
      if(worker.orderStatus == 'pending'){
          html += "<span class=queue><input class=order data-partId="+worker.partId+" data-transType='buy' type='button' value='Buy'></span>";
      }
      else{
          html += "<span class=queue><input class=order data-partId="+worker.partId+" data-transType='cancel' type='button' value='Cancel'></span>";
      }
      // html += "<span class=queue><span class=consumers>"+worker.configuredETA+"</span></span>";
      // html += "<span class=consumers>"+worker.configuredETA+"</span>";
      html += '<span class=queue><span class="status-circle '+worker.orderStatus+'""></span></span>';
     
      html += '</div>';
      g.setNode(id, {
        labelType: "html",
        label: html,
        rx: 5,
        ry: 5,
        padding: 0,
        class: className
      });

      if (worker.parentPartId) {
        g.setEdge(worker.parentPartId, id, {
          width: 45
        });
      }
    }

    inner.call(render, g);

    // Zoom and scale to fit
    var zoomScale = zoom.scale();
    var graphWidth = g.graph().width + 80;
    var graphHeight = g.graph().height + 40;
    var width = parseInt(svg.style("width").replace(/px/, ""));
    var height = parseInt(svg.style("height").replace(/px/, ""));
    zoomScale = Math.min(width / graphWidth, height / graphHeight);
    var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
    zoom.translate(translate);
    zoom.scale(zoomScale);
    zoom.event(isUpdate ? svg.transition().duration(500) : d3.select("svg"));
  }

  // Do some mock queue status updates
  // setInterval(function() {
  //   var stoppedWorker1Count = workers["Job2"].count;
  //   var stoppedWorker2Count = workers["Job5"].count;
  //   for (var id in workers) {
  //     workers[id].count = Math.ceil(Math.random() * 3);
  //     if (workers[id].inputThroughput) workers[id].inputThroughput = Math.ceil(Math.random() * 250);
  //   }
  //   workers["Job2"].count = stoppedWorker1Count + Math.ceil(Math.random() * 100);
  //   workers["Job5"].count = stoppedWorker2Count + Math.ceil(Math.random() * 100);
  //   draw(true);
  //   draw();
  //   if(nodeName){
  //     etlTool.highLightParent(nodeName);
  //     etlTool.highLightsuccessors(nodeName);
  //   }
  // }, 2000);

  // // Do a mock change of worker configuration
  // setInterval(function() {
  //   workers["elasticsearch-monitor"] = {
  //     "consumers": 0,
  //     "count": 0,
  //     "parent": "elasticsearch-writer",
  //     "inputThroughput": 50
  //   }
  // }, 5000);

  draw();

  d3.select('.nodes').selectAll('.node').on('click', function(){
    d3.select('.nodes').selectAll('rect').style('fill','');
    nodeName = d3.select(this).select('.textDiv').attr('data-name');
    etlTool.highLightParent(nodeName);
    etlTool.highLightsuccessors(nodeName);
  });

  // d3.selectAll('.name').on('click', function(ev){
  //   console.log('ev', ev);
  //   window.ev = ev;
  //   $('.popUpDiv').css({top: ev.pageY +'px', left: ev.pageX +'px'});
  // });

  // $('.name').on( 'click', function(ev){
  //   ev.stopPropagation();
  //   $('.popUpDiv').css({top: ev.pageY +'px', left: ev.pageX +'px'});
  //   $('.popUpDiv').css({height : '0px'});
  //   $('.popUpDiv').animate({height : '200px'},500);
  // });

var bindClicks = function(){
  $('.name').on( 'click', function(ev){
    ev.stopPropagation();
    $('.popUpDivForDesc').hide();
    $('.popUpDiv').css({top: ev.pageY+5 +'px', left: ev.pageX-50 +'px', display: 'block'});
    $('.popUpDiv').css({height : '0px'});
    $('.popUpDiv').animate({height : '90px'},500);
    var partId = $(ev.target).closest('.textDiv').attr('data-name');
    $('.popUpDiv').attr("data-name", partId);
  });
  
  $('.order').on( 'click', function(ev){
	    ev.stopPropagation();
	      var partId = $(event.target).attr("data-partId");
	      console.log("part clicked is",partId);
	      var  transType= $(event.target).attr("data-transType");
	      updatePartStatus(partId,transType);	    
	  });
  
  
 /* $('.statusInput').on( 'keyup', function(ev){
    // debugger;
    ev.stopPropagation();
 
    if(ev.keyCode === 13){
      $('.popUpDiv').animate({height : '90px'},500,function(){
        $('.popUpDiv').hide();
      });
      var orderStatus = $(event.target).closest('.popUpDiv').find('select').val();
      var partDesc = $(event.target).val();
      var partId = $('.popUpDiv').attr("data-name");
      var currentNode = hierachyDataObj[partId];
      //currentNode.partDesc = partDesc;
      //currentNode.orderStatus = orderStatus;
      
      console.log('payload', currentNode);
      updatePartStatus(currentNode);
    }
   
  });*/

  $('.name').hover( function(ev){
    ev.stopPropagation();
    $('.popUpDiv').hide();
    $('.popUpDivForDesc').show();
    console.log('in');
    $('.popUpDivForDesc').css({top: ev.pageY+5 +'px', left: ev.pageX-50 +'px', display: 'block'});
   //  $('.popUpDiv').css({height : '0px'});
   //  $('.popUpDiv').animate({height : '90px'},500);

   var desc = $(ev.target).closest('.textDiv').attr('data-desc');
   console.log('desc', desc);
    $('.popUpDivForDesc').text(desc);
  }, function(){
    $('.popUpDivForDesc').hide();
    console.log('out');
  });
}
  bindClicks();

  $('#new').on( 'click', function(ev){
    ev.stopPropagation();
    var partId = $('.popUpDiv').attr("data-name");
    $('.popUpDiv').hide();
    rawArray.push({
      "partDesc": partId+"desc",
      "orderStatus": "pending",
      "partName": partId+"child",
      "partId": partId+"child",
      "parentPartId": partId,
      "price":"NA"
    });
    workers = partDatabase.getConvertedData(rawArray);
    draw(true);
    draw();
    bindClicks();
  });

  

  
  window.updateGraph = function(data){
 workers = workers;
 draw(true);
 draw();

if(nodeName){
      etlTool.highLightParent(nodeName);
      etlTool.highLightsuccessors(nodeName);
    }

  bindClicks();


  }

  


  $(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
        $('.popUpDiv').hide();
    }
});
  