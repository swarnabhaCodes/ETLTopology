package com.hackathon.etl.topology.dto;

public class JobStatus {

	private String jobId;
	private String jobName;
	private String parentJobId;
	private String completionStatus;
	private String completionPercent;
	private String configuredETA;
	private String runningETA;
	private String completedTime;
	private String completionDesc;
	
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getParentJobId() {
		return parentJobId;
	}
	public void setParentJobId(String parentJobId) {
		this.parentJobId = parentJobId;
	}
	public String getCompletionStatus() {
		return completionStatus;
	}
	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}
	public String getCompletionPercent() {
		return completionPercent;
	}
	public void setCompletionPercent(String completionPercent) {
		this.completionPercent = completionPercent;
	}
	public String getConfiguredETA() {
		return configuredETA;
	}
	public void setConfiguredETA(String configuredETA) {
		this.configuredETA = configuredETA;
	}
	public String getRunningETA() {
		return runningETA;
	}
	public void setRunningETA(String runningETA) {
		this.runningETA = runningETA;
	}
	public String getCompletedTime() {
		return completedTime;
	}
	public void setCompletedTime(String completedTime) {
		this.completedTime = completedTime;
	}
	public String getCompletionDesc() {
		return completionDesc;
	}
	public void setCompletionDesc(String completionDesc) {
		this.completionDesc = completionDesc;
	}

	
}
