package com.hackathon.etl.topology.dto;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Part {

	private String partId;
	private String partName;
	private String parentPartId;
	private String price;
	private String level;
	private String partDesc;
	private String orderStatus;
	
  public String getPartId() {
    return partId;
  }
  public void setPartId(String partId) {
    this.partId = partId;
  }
  public String getPartName() {
    return partName;
  }
  public void setPartName(String partName) {
    this.partName = partName;
  }
  public String getParentPartId() {
    return parentPartId;
  }
  public void setParentPartId(String parentPartId) {
    this.parentPartId = parentPartId;
  }
  public String getPrice() {
    return price;
  }
  public void setPrice(String price) {
    this.price = price;
  }
  public String getLevel() {
    return level;
  }
  public void setLevel(String level) {
    this.level = level;
  }
  public String getPartDesc() {
    return partDesc;
  }
  public void setPartDesc(String partDesc) {
    this.partDesc = partDesc;
  }
  public String getOrderStatus() {
    return orderStatus;
  }
  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }
	
}