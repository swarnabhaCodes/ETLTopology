package com.hackathon.etl.topology.git;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.hackathon.etl.topology.dto.ResponseDTO;

@Component("gitCheckInExecutor")
public class GitPushExecutorImpl implements IGitPushExecutor, DisposableBean {
  protected ListeningExecutorService executorService;

  public GitPushExecutorImpl() {
    this.executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
  }

  @Override
  public ListenableFuture<ResponseDTO> pushToGitAsync(final JSONObject gitPushArtifactMap) {
    ListenableFuture<ResponseDTO> listenableFuture =
        executorService.submit(new Callable<ResponseDTO>() {
          @Override
          public ResponseDTO call() throws Exception {
            // pushArtifactToGit(processedMessageRequest, producer);
            ResponseDTO responseDTO= pushToGit(gitPushArtifactMap);
            return responseDTO;
          }
        });
    return listenableFuture;

  }

  public ResponseDTO pushToGit( JSONObject gitTemplateJSON) {
    System.out.println("pushing to git using Git push executor");
    ResponseDTO responseDTO = new ResponseDTO();
    responseDTO.setCode("code1"+Thread.currentThread().getId());
    responseDTO.setMessage("message");
    System.out.println("working on a task"+gitTemplateJSON.toJSONString());
    Integer maxCount = (Integer)gitTemplateJSON.get("count");
    String name = (String)gitTemplateJSON.get("name");
    int i = 0;
    while (i < maxCount){
      try{
        System.out.println("sleeping work"+name);
        if(name.startsWith("app")){
          Thread.currentThread().sleep(500);
        }
        else{
          Thread.currentThread().sleep(1000);
        }
          
         // System.out.println("working counter for job name= "+name+" count is "+i+"thread id is"+Thread.currentThread().getId());
         // System.out.println("gitTemplateJSON content"+gitTemplateJSON.get("content"));
 
      }catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }    
      i++;
    }
    return responseDTO;

  }

  public void shutDown() {
    this.executorService.shutdownNow();
  }

  @Override
  public void destroy() throws Exception {
    shutDown();

  }
}
