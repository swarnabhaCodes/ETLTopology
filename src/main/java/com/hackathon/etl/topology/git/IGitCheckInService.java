/**
 * 
 */
package com.hackathon.etl.topology.git;

import java.util.List;
import java.util.Map;

import com.hackathon.etl.topology.dto.ResponseDTO;

public interface IGitCheckInService {
	
	public List<ResponseDTO> checkinConfigToGit(Map<String, List<String>> dataResult);

}
