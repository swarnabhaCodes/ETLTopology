package com.hackathon.etl.topology.git;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.collect.Iterables;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hackathon.etl.topology.dto.ResponseDTO;

@Service("gitCheckInService")
public class GitCheckInService implements IGitCheckInService{
  
  @Autowired
  @Qualifier("gitCheckInExecutor")
  private IGitPushExecutor gitCheckInExecutor;
  
  public List<ResponseDTO> checkinConfigToGit(Map<String, List<String>> dataResult) {
    long startTime = System.currentTimeMillis();
      final List<ResponseDTO> result = new ArrayList<ResponseDTO>();
      List<ResponseDTO> dtos = new ArrayList<>();
      //      JSONObject jsonObject = null;
      List<ListenableFuture<ResponseDTO>> listenableFutures = new ArrayList<ListenableFuture<ResponseDTO>>();
      for (Map.Entry<String, List<String>> entry : dataResult.entrySet()) {
          String artefactType = entry.getKey();
          List<String> mapValueKey = entry.getValue();

          for (String dataResult2 : mapValueKey) {
              final JSONObject gitTemplateJSON = new JSONObject();
              final String artifactJson = dataResult2;

              try {
                gitTemplateJSON.put("name", artifactJson);
                  gitTemplateJSON.put("content", artifactJson);
//                  if (artefactType.equalsIgnoreCase("ds")){
                    gitTemplateJSON.put("count", 10);
//                  }
                 
                  gitTemplateJSON.put("artefactType", artefactType);
                  // TODO submit to the callable task
                  ListenableFuture<ResponseDTO> lf =gitCheckInExecutor.pushToGitAsync(gitTemplateJSON);
                  listenableFutures.add(lf);
//                  ResponseDTO responseDTO= gitCheckInExecutor.pushToGit(gitTemplateJSON);
//                  dtos.add(responseDTO);
              } catch (Exception ex) {
                ex.printStackTrace();
                
              }
          }
      }
      ListenableFuture<List<ResponseDTO>> lf = Futures.successfulAsList(listenableFutures);
      
      Futures.addCallback(lf, new FutureCallback<List<ResponseDTO>>() {
        @Override
        public void onSuccess(List<ResponseDTO> jobResults) {
          System.out.println("@@ finished processing {} elements"+ Iterables.size(jobResults));
            // do something with all the results
          for(ResponseDTO responseDTO : jobResults){
            result.add(responseDTO);
          }
        }

        @Override
        public void onFailure(Throwable t) {
          t.printStackTrace();
          System.out.println("@@ failed because of :: {}"+ t.getMessage());
        }
    });
      try {
        dtos = lf.get();
        System.out.println("lf get"+dtos.size());
      } catch (InterruptedException | ExecutionException e1) {
        // TODO Auto-generated catch blocker
        System.out.println("error in thread"+ e1.getMessage());
        e1.printStackTrace();
      }
//      while(!lf.isDone()){
//        System.out.println("all not done"+result.size());
//        try {
//          Thread.currentThread().sleep(8000);
//        } catch (InterruptedException e) {
//          // TODO Auto-generated catch block
//          e.printStackTrace();
//        }
//      }
      System.out.println("all tasks done"+dtos.size());
      System.out.println("time taken"+(System.currentTimeMillis() - startTime));
      return dtos;
  }
}
