package com.hackathon.etl.topology.git;

import org.json.simple.JSONObject;

import com.google.common.util.concurrent.ListenableFuture;
import com.hackathon.etl.topology.dto.ResponseDTO;

/**
 * Defines contract for posting kafka message
 *
 * @author swarnabha_lahiri
 *
 */
public interface IGitPushExecutor {

  /**
   * Receives the processed message request having the final message to be posted (prepared) and
   * ensure that the message is posted to the topic using the kafka producer
   *
   * @param processedMessageRequest
   * @param producer
   * @return ListenableFuture<Void>
   */
  ListenableFuture<ResponseDTO> pushToGitAsync(JSONObject artifactJson);
  
  ResponseDTO pushToGit( JSONObject gitTemplateJSON);

}
