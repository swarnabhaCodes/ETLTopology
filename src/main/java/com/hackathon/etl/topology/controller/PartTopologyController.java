package com.hackathon.etl.topology.controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hackathon.etl.topology.dto.JobStatus;
import com.hackathon.etl.topology.dto.Part;

@Controller
public class PartTopologyController {

	@Autowired
	@Qualifier("partsRetriever")
	private PartListPublisher partsPublisher;
	
	@RequestMapping(value = "/neo", method = RequestMethod.GET)
    public String neoIndex() {
        return "neo4jIndex";
    }

	
	@SubscribeMapping("/partList")
    public Object partSubscription() throws IOException, ParseException {
        System.out.println("part topic subscribed");
        //jobStatusPublisher.publishJobStatus();
        return partsPublisher.getParts();
    }
	
	@MessageMapping("/updatePart" )
    @SendTo("/topic/partList")
    @RequestMapping(value = "/updatePart", method = RequestMethod.POST)
    public ModelAndView updatePart(@RequestBody String partData,HttpServletResponse response) throws FileNotFoundException, IOException, ParseException {
        //return jobStatusPublisher.updateJob(jobStatus);
//         return JsonView.Render(partsPublisher.updatePart(partId), response);
    return JsonView.Render(partsPublisher.updateNeoPart(partData), response);

    }
	
//	@MessageMapping("/updateNeoPart" )
//    @SendTo("/topic/partList")
//    @RequestMapping(value = "/updateNeoPart", method = RequestMethod.POST)
//    public ModelAndView updateNeoPart(@RequestBody String partId,HttpServletResponse response) throws FileNotFoundException, IOException, ParseException {
//        //return jobStatusPublisher.updateJob(jobStatus);
//         return JsonView.Render(partsPublisher.updateNeoPart(partId), response);
//    }
	
	@RequestMapping(value = "/getParts", method = RequestMethod.GET)
	public ModelAndView getParts(HttpServletResponse response) throws FileNotFoundException, IOException, ParseException {
		//return jobStatusPublisher.updateJob(jobStatus);
		 return JsonView.Render(partsPublisher.getParts(), response);
	}
	@RequestMapping(value = "/getNeoParts", method = RequestMethod.GET)
    public ModelAndView getNeoParts(HttpServletResponse response) throws FileNotFoundException, IOException, ParseException {
        //return jobStatusPublisher.updateJob(jobStatus);
         return JsonView.Render(partsPublisher.getNeoParts(), response);
    }
	
}
