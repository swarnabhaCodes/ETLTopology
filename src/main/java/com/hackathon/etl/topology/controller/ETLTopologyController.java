package com.hackathon.etl.topology.controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hackathon.etl.topology.dto.JobStatus;

@Controller
public class ETLTopologyController {

	@Autowired
	@Qualifier("jobStatusRetriever")
	private JobStatusPublisher jobStatusPublisher;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	@SubscribeMapping("/jobList")
	public Object jobSubscription() throws IOException, ParseException {
		System.out.println("tpoic subscribed");
		//jobStatusPublisher.publishJobStatus();
		return jobStatusPublisher.getJobs();
	}
	
	@MessageMapping("/updateJob" )
	@SendTo("/topic/jobList")
	@RequestMapping(value = "/updateJob", method = RequestMethod.POST)
	public ModelAndView updateJob(@RequestBody JobStatus jobStatus,HttpServletResponse response) throws FileNotFoundException, IOException, ParseException {
		//return jobStatusPublisher.updateJob(jobStatus);
		 return JsonView.Render(jobStatusPublisher.updateJob(jobStatus), response);
	}
	
	
	@RequestMapping(value = "/getJobs", method = RequestMethod.GET)
	public ModelAndView getJobs(HttpServletResponse response) throws FileNotFoundException, IOException, ParseException {
		//return jobStatusPublisher.updateJob(jobStatus);
		 return JsonView.Render(jobStatusPublisher.getJobs(), response);
	}
	
}
