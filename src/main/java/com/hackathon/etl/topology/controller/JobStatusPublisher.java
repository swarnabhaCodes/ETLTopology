package com.hackathon.etl.topology.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.hackathon.etl.topology.dto.JobStatus;

@Service("jobStatusRetriever")
public class JobStatusPublisher {
	@Autowired
	private SimpMessagingTemplate template;

	//@Scheduled(fixedDelay = 10000)
	public void publishJobStatus() throws IOException, ParseException {
		// TODO get Job status from DB or file
		System.out.println("publishing status publishJobStatus");
		JSONParser jsonParser = new JSONParser();
		Object obj = jsonParser.parse(new FileReader("/Users/swarnabha_lahiri/SourceCode/POCs/ETLTopology/src/main/resources/jobStatus.json"));
		System.out.println("input stram received");
		// InputStream is =
		// getClass().getResourceAsStream(
		// "/src/main/resources/jobStatus.json");
		System.out.println("input stram received" + obj);
		// ObjectMapper mapper = new ObjectMapper();
		// List<JobStatus> jobLst = mapper.readValue(jsonTxt.getBytes(),
		// TypeFactory.defaultInstance().constructCollectionType(List.class,
		// JobStatus.class));
		System.out.println("publishing status with json " + obj);
		// TODO publish status to subscriber
		template.convertAndSend("/topic/jobStatus", obj);
	}
	
	
	public Object getJobs() throws IOException, ParseException {
		// TODO get Job status from DB or file
		System.out.println("publishing status publishJobStatus");
		JSONParser jsonParser = new JSONParser();
		Object obj = jsonParser.parse(new FileReader("/Users/swarnabha_lahiri/SourceCode/POCs/ETLTopology/src/main/resources/jobStatus.json"));
		// InputStream is =
		// getClass().getResourceAsStream(
		// "/src/main/resources/jobStatus.json");
		System.out.println("input stram received" + obj);
		// ObjectMapper mapper = new ObjectMapper();
		// List<JobStatus> jobLst = mapper.readValue(jsonTxt.getBytes(),
		// TypeFactory.defaultInstance().constructCollectionType(List.class,
		// JobStatus.class));
		System.out.println("publishing status with json " + obj);
		// TODO publish status to subscriber
		return obj;
	}
	
	public String updateJob(JobStatus jobStatus) throws FileNotFoundException, IOException, ParseException{
		String status = "fail";
		JSONParser jsonParser = new JSONParser();
		Object obj = jsonParser.parse(new FileReader("/Users/swarnabha_lahiri/SourceCode/POCs/ETLTopology/src/main/resources/jobStatus.json"));
		System.out.println("updateJob input stram received");
		// InputStream is =
		// getClass().getResourceAsStream(
		// "/src/main/resources/jobStatus.json");
		System.out.println("updateJob obj" + obj);
		JSONArray jsonArray = (JSONArray)obj;
		for (Object jsonObject : jsonArray){
			JSONObject item = (JSONObject)jsonObject;
			if( null != item.get("jobId")){
				System.out.println("Job id is"+item);
				if (((String)item.get("jobId")).equalsIgnoreCase(jobStatus.getJobId())){
					item.put("completionStatus", jobStatus.getCompletionStatus());
					item.put("jobName", jobStatus.getJobName());
					item.put("completionDesc", jobStatus.getCompletionDesc());
				}
			}
		}
		FileWriter file = new FileWriter("/Users/swarnabha_lahiri/SourceCode/POCs/ETLTopology/src/main/resources/jobStatus.json");
        try {
            file.write(jsonArray.toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
            status="fail";
        } finally {
            file.flush();
            file.close();
        }
        template.convertAndSend("/topic/jobStatus", jsonArray);
        status = "success";
		return status;
	}

}
