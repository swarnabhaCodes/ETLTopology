package com.hackathon.etl.topology.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.hackathon.etl.topology.dto.Part;
import com.hackathon.etl.topology.repository.PartsDao;

@Service("partsRetriever")
//@EnableScheduling
public class PartListPublisher {
  @Autowired
  private SimpMessagingTemplate template;
  
  private TaskScheduler scheduler = new ConcurrentTaskScheduler();

  @Autowired
  @Qualifier("partsDao")
  private PartsDao partsDao;

//  @Scheduled(fixedDelay = 10000)
  public void publishPartsData() throws IOException, ParseException {
    // TODO get Job status from DB or file
    System.out.println("parts  publishParts");
//    JSONParser jsonParser = new JSONParser();
//    Object obj = jsonParser.parse(new FileReader(
//        "/Users/swarnabha_lahiri/SourceCode/POCs/ETLTopology/src/main/resources/neoParts.json"));
//    System.out.println("input stram received");
    // InputStream is =
    // getClass().getResourceAsStream(
    // "/src/main/resources/jobStatus.json");
//    System.out.println("Neo input stram received" + obj);
    // ObjectMapper mapper = new ObjectMapper();
    // List<JobStatus> jobLst = mapper.readValue(jsonTxt.getBytes(),
    // TypeFactory.defaultInstance().constructCollectionType(List.class,
    // JobStatus.class));
//    System.out.println("publishing status with json " + obj);
    // TODO publish status to subscriber
   ;
    template.convertAndSend("/topic/partList",  getNeoParts());
  }


  public Object getParts() throws IOException, ParseException {
    // TODO get Job status from DB or file
    System.out.println("publishing status publishJobStatus");
//    JSONParser jsonParser = new JSONParser();
//    Object obj = jsonParser.parse(new FileReader(
//        "/Users/swarnabha_lahiri/SourceCode/POCs/ETLTopology/src/main/resources/neoParts.json"));
    // InputStream is =
    // getClass().getResourceAsStream(
    // "/src/main/resources/jobStatus.json");
//    System.out.println("input stram received" + obj);
    // ObjectMapper mapper = new ObjectMapper();
    // List<JobStatus> jobLst = mapper.readValue(jsonTxt.getBytes(),
    // TypeFactory.defaultInstance().constructCollectionType(List.class,
    // JobStatus.class));
//    System.out.println("publishing status with json " + obj);
    // TODO publish status to subscriber
    return getNeoParts();
  }

  public String updatePart(String partId)
      throws FileNotFoundException, IOException, ParseException {
    String status = "fail";
    JSONParser jsonParser = new JSONParser();
    Object obj = jsonParser.parse(new FileReader(
        "/Users/swarnabha_lahiri/SourceCode/POCs/ETLTopology/src/main/resources/neoParts.json"));
    System.out.println("updatePart input stram received");
    // InputStream is =
    // getClass().getResourceAsStream(
    // "/src/main/resources/jobStatus.json");
    System.out.println("updatePart obj" + obj);
    JSONArray jsonArray = (JSONArray) obj;
    for (Object jsonObject : jsonArray) {
      JSONObject item = (JSONObject) jsonObject;
      if (null != item.get("partId")) {
        System.out.println("part id is" + item);
        if (((String) item.get("partId")).equalsIgnoreCase(partId)) {
          item.put("orderStatus", "ordered");
        }
      }
    }
    FileWriter file = new FileWriter(
        "/Users/swarnabha_lahiri/SourceCode/POCs/ETLTopology/src/main/resources/neoParts.json");
    try {
      file.write(jsonArray.toJSONString());
    } catch (Exception e) {
      e.printStackTrace();
      status = "fail";
    } finally {
      file.flush();
      file.close();
    }
    template.convertAndSend("/topic/partList", jsonArray);
    status = "success";
    return status;
  }
  
  public String updateNeoPart(String partData) throws JsonParseException, JsonMappingException, MessagingException, ParseException, IOException{
    String part[] = partData.split(":");
    String partId=part[0];
    String transType=part[1];
    String status= partsDao.updatePart(partId,transType);
    template.convertAndSend("/topic/partList", getNeoParts());
    return status;
  }


  public List<?> getNeoParts() throws ParseException, JsonParseException, JsonMappingException, IOException {
    List<JSONObject> jsonList = (ArrayList<JSONObject>) partsDao.getParts();
//    List<Part> myObjects =
//        mapper.readValue(jsonInput, new TypeReference<List<Part>>(){});
    ObjectMapper mapper = new ObjectMapper();
    JSONParser jsonParser = new JSONParser();
    // Map<String,Object> map = jsonList.get(0);
    // Map<String,Object> partNodeMap = (Map<String,Object>)map.get("node");
    // Map<String,Object> parentPartNodeMap = (Map<String,Object>)map.get("parent");
    // partNodeMap.put("parentPartId", parentPartNodeMap.get("partId"));
    // System.out.println(partNodeMap);
    List<Part> parts = new ArrayList<>();
    for (Map<String, Object> partElementMap : jsonList) {
      Map<String, Object> partNodeMap = (Map<String, Object>) partElementMap.get("node");
      Map<String, Object> parentPartNodeMap = (Map<String, Object>) partElementMap.get("parent");
        if (null != parentPartNodeMap.get("partId")){
          partNodeMap.put("parentPartId", parentPartNodeMap.get("partId"));
          String jsonString = new Gson().toJson(partNodeMap);
          Part part = mapper.readValue(jsonString.getBytes(), Part.class);
          parts.add(part);
        }
    }
    return parts;
  }
  
  @PostConstruct
  private void broadcastPartsListPeriodically() {
    scheduler.scheduleAtFixedRate(new Runnable() {
      @Override public void run() {
        try {
          publishPartsData();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (ParseException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }, 5000);
  }
}
