package com.hackathon.etl.topology.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hackathon.etl.topology.dto.ResponseDTO;
import com.hackathon.etl.topology.git.IGitCheckInService;
@Controller
public class GitCheckInController {

  @Autowired
  @Qualifier("gitCheckInService")
  private IGitCheckInService gitCheckInService;
  
  @RequestMapping(value = "/checkIn", method = RequestMethod.GET)
  public ModelAndView getNeoParts(HttpServletResponse response) throws FileNotFoundException, IOException, ParseException {
      //return jobStatusPublisher.updateJob(jobStatus);
    Map<String,List<String>> dataResult = new HashMap<>();
    List<String> appList = new ArrayList<>();
    appList.add("app1");
    appList.add("app2");
    appList.add("app3");
    dataResult.put("app", appList);
    List<String> datasrcList = new ArrayList<>();
    datasrcList.add("ds1");
    datasrcList.add("ds2");
    datasrcList.add("ds3");
    dataResult.put("ds", datasrcList);
    List<ResponseDTO> dtos = gitCheckInService.checkinConfigToGit(dataResult);
    System.out.println("Retrieved results are"+dtos.size());
    for (ResponseDTO rsResponseDTO: dtos){
      System.out.println("responsed code is"+rsResponseDTO.getCode());
    }
       return JsonView.Render(dtos, response);
  }
}
