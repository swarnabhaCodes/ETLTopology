package com.hackathon.etl.topology.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("partsDao")
public class PartsDao {

  @Autowired
  private JdbcTemplate neoJdbcTemplate;

  public List<?> getParts() {
    return neoJdbcTemplate.queryForList("MATCH (node:Part)-->(parent) RETURN *");

    // parts = neoJdbcTemplate.query("match(n:Part) return *",new RowMapper<Part>(){
    // @Override
    // public Part mapRow(ResultSet rs, int rownumber)throws SQLException {
    // Part part = new Part();
    // part.setPartId(rs.getString("n"));
    // part.setPartName(rs.getString("id"));
    // return part;
    // }
    // });
    //
    // return parts;
  }

  public String updatePart(String partId,String transType) {
    if (transType.equalsIgnoreCase("cancel")){
      neoJdbcTemplate
      .update("MATCH (n:Part{ partId:'" + partId + "'}) SET n.orderStatus='pending' RETURN n");
    }
    else{
      neoJdbcTemplate
      .update("MATCH (n:Part{ partId:'" + partId + "'}) SET n.orderStatus='ordered' RETURN n");
    }
    return "success";
  }

}
